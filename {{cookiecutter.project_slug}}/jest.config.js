module.exports = {
  roots: ["<rootDir>/test"],
  setupFilesAfterEnv: ["./test/setupJest.js"],
  snapshotSerializers: ["enzyme-to-json/serializer"],
  testRegex: "(/__tests__/.*|(\\.|/)(test|spec))\\.jsx?$",
  moduleFileExtensions: ["js", "jsx", "json"],
  reporters: ["default", "jest-junit"]
}
