const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');

module.exports = {
  mode: 'development',
  entry: './src/index.js',
  plugins: [new HtmlWebpackPlugin()],
  module: {
    rules: [
      {
        test: /^(?!.*\.spec\.js$).*\.js$/,
        exclude: [path.join(__dirname, 'node_modules')],
        use: {
          loader: 'babel-loader',
        }
      },
    ],
  },
};
