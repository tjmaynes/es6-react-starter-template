import React from "react";
import { shallow, mount } from "enzyme";
import App from "../../src/components/App";

describe("#App", () => {
  let wrapper;

  beforeEach(() => {
    wrapper = shallow(<App />);
  });

  it("renders a <div>", () => {
    expect(wrapper.type()).toBe("div");
  });

  it("should render an <h1 /> tag", () => {
    expect(wrapper.contains(<h1>Hello World</h1>)).toBe(true);
  });
});
